
# Rofi Keepass

Use Rofi to quickly autofill passwords.

Buit apon [Rust KeePass Cli](https://gitlab.com/ari-donnenfeld/rust-keepass-cli).

## Usage:

```
rofi-keepass.sh [OPTIONS]

  arguments:
  -h, --help                show this help message and exit
  -d, --database [file]     specify keepass database file path
  -t, --theme [file]        specify rofi theme file path
```

e.g.
```bash
rofi_keepass.sh -d ~/passwords.kdbx -t ~/colours.rasi
```
