#!/bin/sh


m() { rofi -dmenu -i "$@" ;}

copy() {
    echo "$1" | xclip -sel clip
}

autofill() {
    xdotool type --delay 10 "$username";
    xdotool key Tab;
    xdotool type --delay 10 "$password";
}
fill() {
    xdotool type --delay 10 "$1";
}

print_help() {
  printf "usage: rofi-keepass.sh [-h] [-d]

  arguments:
  -h, --help                show this help message and exit
  -d, --database [file]     specify keepass database file path
  -t, --theme [file]        specify rofi theme file path\n\n"
}

[ ! "$1" ] && { print_help; exit ;}
while [ "$1" ]; do
  case "$1" in
    -h | --help) print_help; exit;;
    -d | --database) db="$2"; shift; shift;;
    -t | --theme) theme="$2"; shift; shift;;
    *) echo "[E] Invalid argument."; exit 1;;
  esac
done

while [ "$2" ]; do
  case "$2" in
    -h | --help) print_help; exit;;
    -d | --database) db="$3"; shift; shift;;
    -t | --theme) theme="$3"; shift; shift;;
    *) echo "[E] Invalid argument."; exit 1;;
  esac
done


dbpass=$(m -p "Enter your database password:" -l 0 -password -width 500 -theme $theme)
[ ! "$dbpass" ] && exit


entries=$(echo "$dbpass" | /home/red/Documents/rkp_cli/rkp_cli/target/release/rkp_cli -l -d $db | tail -n +2)

[ ! "$entries" ] && rofi -e "Error. Either Path or Password are incorrect." -theme $theme && exit

selection=$(echo "$entries" | rofi -dmenu -i  -p "Select Entry:" -width 350 -theme $theme | cut -d ":" -f1)
[ ! "$selection" ] && exit


details=$(echo "$dbpass" | /home/red/Documents/rkp_cli/rkp_cli/target/release/rkp_cli -d $db -s $selection | tail -n +2)

title=$(echo "$details" | sed -n '1p' | cut -d " " -f2-)
username=$(echo "$details" | sed -n '2p' | cut -d " " -f2-)
password=$(echo "$details" | sed -n '3p' | cut -d " " -f2-)

action=$(printf "Autofill\nCopy Username\nCopy Password\nFill Password\nAutofill with Pause" | m -p "Choose action:" -l 5 -theme $theme)
[ ! "$action" ] && exit


case "$action" in
    Autofill) autofill;;
    "Copy Username") copy "$username";;
    "Copy Password") copy "$password";;
    "Fill Password") fill "$password";;
    "Autofill with Pause") fill "$username" && sleep 3 && fill "$password";;
esac


